
/*
 * (c) Copyright 2020 Brite:Bill Ltd.
 *
 * 7 Grand Canal Street Lower, Dublin 2, Ireland
 * info@britebill.com
 * +353 1 661 9426
 */
package com.amdocs;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author <a href="mailto:razvanm@amdocs.com">Răzvan Mihai</a>
 */
public class IncrementTest {

    @Test
    public void testGetCounter() {
        Increment incr = new Increment();

        assertEquals(1, incr.getCounter());
        assertEquals(2, incr.getCounter());
    }
}

