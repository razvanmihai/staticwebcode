
/*
 * (c) Copyright 2020 Brite:Bill Ltd.
 *
 * 7 Grand Canal Street Lower, Dublin 2, Ireland
 * info@britebill.com
 * +353 1 661 9426
 */
package com.amdocs;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author <a href="mailto:razvanm@amdocs.com">Răzvan Mihai</a>
 */
public class CalculatorTest {

    @Test
    public void testAdd() {
        assertEquals(new Calculator().add(), 9);
    }

    @Test
    public void testSub() {
        assertEquals(new Calculator().sub(), 3);
    }
}

